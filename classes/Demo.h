
//header file (.h) contains class definition
 class Demo
 {
    private:  //properties
         int var;
         double time;

    public:   //methods
        
         Demo();//default constructor (no parameters)
         Demo(int, double);// "overloaded" constructor

          void setVar(int);// set (change) the value of var
          int getVar();// view contents of variable var
          void setTime(double);
          double getTime();           

 };



